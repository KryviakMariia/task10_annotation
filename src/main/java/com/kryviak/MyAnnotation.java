package com.kryviak;

import java.lang.annotation.*;

@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface MyAnnotation {
    String name();

    String type() default "animal";

    int weight();
}
