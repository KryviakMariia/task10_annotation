package com.kryviak;

public class MyClass {

    @MyAnnotation(name = "Animal", weight = 5)
    private String number = "First";

    @MyAnnotation(name = "Person", weight = 100, type = "people")
    private int age;

    public String getNumber(){
        return number;
    }

    private String names() {
    return "Person1";
    }

    public void printInfo(String number, int age) {
        System.out.println(number + age);
    }
}
