package com.kryviak;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Main {

    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        Class clazz = myClass.getClass();
        String number = "number";
        String age = "age";

        try {
            setNewName(clazz);
            printAnnotation(clazz, age);
            printAnnotation(clazz, number);
        } catch (IllegalAccessException e) {
            System.out.println("Illegal Access Exception");
        } catch (InstantiationException e) {
            System.out.println("Instantiation Exception");
        } catch (NoSuchFieldException e) {
            System.out.println("No Such Field Exception");
        }

        System.out.println(clazz.getName());
        System.out.println(clazz.getSimpleName());

        System.out.println("Is modifier Private? " + Modifier.isPrivate(clazz.getModifiers()));
        System.out.println("Is modifier Public? " + Modifier.isPublic(clazz.getModifiers()));
        System.out.println(clazz.getPackage());
    }

    private static void setNewName(Class clazz) throws IllegalAccessException, InstantiationException, NoSuchFieldException {
        MyClass sc = (MyClass) clazz.newInstance();

        Field declaredFields = clazz.getDeclaredField("number");

        declaredFields.setAccessible(true);

        String test = (String) declaredFields.get(sc);
        System.out.println("Field before SET: " + sc.getNumber());

        declaredFields.set(sc, "Second");
        System.out.println("Field after SET: " + sc.getNumber());
    }

    private static void printAnnotation(Class clazz, String fieldOfMyClass) throws NoSuchFieldException {
        Field declaredFields = clazz.getDeclaredField(fieldOfMyClass);
        MyAnnotation anno = declaredFields.getAnnotation(MyAnnotation.class);
        System.out.println(anno.name() + " " + anno.type() + " " + anno.weight());
    }


}
